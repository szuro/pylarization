import unittest
from numpy import sqrt, pi
from pylarization.matrices import CoherencyMatrix
from pylarization.ellipse import PolarizationEllipse
from pylarization.polarizations import CoherencyMatrixState, PolarizationEllipseState
from tests import DummyVector


class TestCoherencyMatrixValues(unittest.TestCase):
    def setUp(self):
        self.linear_horizontal = CoherencyMatrixState.LINEAR_HORIZONTAL.value
        self.linear_vertical = CoherencyMatrixState.LINEAR_VERTICAL.value
        self.linear_plus45 = CoherencyMatrixState.LINEAR_DIAGONAL.value
        self.linear_minus45 = CoherencyMatrixState.LINEAR_ANTIDIAGONAL.value
        self.circular_left_handed = CoherencyMatrixState.CIRCULAR_LEFT_HANDED.value
        self.circular_right_handed = CoherencyMatrixState.CIRCULAR_RIGHT_HANDED.value

    def test_E0x(self):
        self.assertAlmostEqual(self.linear_horizontal.E0x, PolarizationEllipseState.LINEAR_HORIZONTAL.value.E0x)
        self.assertAlmostEqual(self.linear_vertical.E0x, PolarizationEllipseState.LINEAR_VERTICAL.value.E0x)
        self.assertAlmostEqual(self.linear_plus45.E0x, PolarizationEllipseState.LINEAR_DIAGONAL.value.E0x)
        self.assertAlmostEqual(self.linear_minus45.E0x, PolarizationEllipseState.LINEAR_ANTIDIAGONAL.value.E0x)
        self.assertAlmostEqual(self.circular_left_handed.E0x, PolarizationEllipseState.CIRCULAR_LEFT_HANDED.value.E0x)
        self.assertAlmostEqual(self.circular_right_handed.E0x, PolarizationEllipseState.CIRCULAR_RIGHT_HANDED.value.E0x)

    def test_E0y(self):
        self.assertAlmostEqual(self.linear_horizontal.E0y, PolarizationEllipseState.LINEAR_HORIZONTAL.value.E0y)
        self.assertAlmostEqual(self.linear_vertical.E0y, PolarizationEllipseState.LINEAR_VERTICAL.value.E0y)
        self.assertAlmostEqual(self.linear_plus45.E0y, PolarizationEllipseState.LINEAR_DIAGONAL.value.E0y)
        self.assertAlmostEqual(self.linear_minus45.E0y, PolarizationEllipseState.LINEAR_ANTIDIAGONAL.value.E0y)
        self.assertAlmostEqual(self.circular_left_handed.E0y, PolarizationEllipseState.CIRCULAR_LEFT_HANDED.value.E0y)
        self.assertAlmostEqual(self.circular_right_handed.E0y, PolarizationEllipseState.CIRCULAR_RIGHT_HANDED.value.E0y)

    def test_phase(self):
        self.assertAlmostEqual(self.linear_horizontal.phase, PolarizationEllipseState.LINEAR_HORIZONTAL.value.phase)
        self.assertAlmostEqual(self.linear_vertical.phase, PolarizationEllipseState.LINEAR_VERTICAL.value.phase)
        self.assertAlmostEqual(self.linear_plus45.phase, PolarizationEllipseState.LINEAR_DIAGONAL.value.phase)
        self.assertAlmostEqual(self.linear_minus45.phase, PolarizationEllipseState.LINEAR_ANTIDIAGONAL.value.phase)
        self.assertAlmostEqual(self.circular_left_handed.phase, PolarizationEllipseState.CIRCULAR_LEFT_HANDED.value.phase)
        self.assertAlmostEqual(self.circular_right_handed.phase, PolarizationEllipseState.CIRCULAR_RIGHT_HANDED.value.phase)


class TestCoherencyMatrixAddition(unittest.TestCase):
    def setUp(self):
        self.circular_left_handed = CoherencyMatrixState.CIRCULAR_LEFT_HANDED.value
        self.circular_right_handed = CoherencyMatrixState.CIRCULAR_RIGHT_HANDED.value

    def test_addition(self):
        expected_sum = CoherencyMatrix(1, 0, 0, 1)
        matrix_sum = self.circular_right_handed + self.circular_left_handed
        self.assertEqual(expected_sum.matrix.all(), matrix_sum.matrix.all())


class TestCoherencyMatrixDegreeOfPolarization(unittest.TestCase):
    def test_degree_of_polarization(self):
        for name, member in CoherencyMatrixState.__members__.items():
            with self.subTest(state=name):
                self.assertAlmostEqual(member.value.degree_of_polarization, 1)


if __name__ == '__main__':
    unittest.main()
