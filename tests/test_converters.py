import unittest
from pylarization.vectors import JonesVector, StokesVector
from pylarization.matrices import CoherencyMatrix
from pylarization.polarizations import *
from pylarization.converters import *


class TestConverters(unittest.TestCase):
    def subtest_helper(self, original, target, converter):
        for name, member in original.__members__.items():
            with self.subTest(state=name):
                output = converter(member.value)
                self.assertAlmostEqual(output.E0x, target[name].value.E0x)
                self.assertAlmostEqual(output.E0y, target[name].value.E0y)
                self.assertAlmostEqual(output.phase, target[name].value.phase)

    def test_stokes_to_jones(self):
        self.subtest_helper(StokesVectorState, JonesVectorState, stokes_to_jones)

    def test_jones_to_stokes(self):
        self.subtest_helper(JonesVectorState, StokesVectorState, jones_to_stokes)

    def test_stokes_to_coherency(self):
        self.subtest_helper(StokesVectorState, CoherencyMatrixState, stokes_to_coherency)

    def test_coherency_to_stokes(self):
        self.subtest_helper(CoherencyMatrixState, StokesVectorState, coherency_to_stokes)

    def test_jones_to_coherency(self):
        self.subtest_helper(JonesVectorState, CoherencyMatrixState, jones_to_coherency)

    def test_coherency_to_jones(self):
        self.subtest_helper(CoherencyMatrixState, JonesVectorState, coherency_to_stokes)


if __name__ == '__main__':
    unittest.main()
